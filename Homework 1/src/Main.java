import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        String[][] randomQuestions = {
                {"What is the square root of 64?", "8"},
                {"How many sides does a hexagon have?", "6"},
                {"What is the sum of 15 and 27?", "42"},
                {"How many centimeters are in a meter?", "100"},
                {"What is the atomic number of oxygen?", "8"},
                {"How many degrees are in a right angle?", "90"},
                {"If a rectangle has a length of 10 units and a width of 5 units, what is its area?", "50"},
                {"What is the freezing point of water in Celsius?", "0"},
                {"How many planets are in our solar system?", "8"},
        };

        System.out.print("Enter your name: ");
        String userName = scanner.nextLine();
        System.out.println("Let the game begin!");

        while (true) {
            // Отримання індексу рандомного питання
            int numberRandomQuestion = random.nextInt(0, randomQuestions.length);
            // Радномне питання
            String question = randomQuestions[numberRandomQuestion][0];
            // Відповідь на рандомне питання
            int answer = Integer.parseInt(randomQuestions[numberRandomQuestion][1]);

            // Отримання відповіді на питання від користувача
            int userNumber;
            // Збереження неправильний відповідей користувача
            String failedNumbers = "";

            do {
                System.out.print(question + " ");

                // Обровка введення нечислового значення від користувача
                while (!scanner.hasNextInt()) {
                    System.out.println("Введено нечислове значення. Спробуйте ще раз.");
                    System.out.println();
                    System.out.print(question + " ");
                    scanner.next();
                }
                userNumber = scanner.nextInt();

                // Обровка чи правильно користувач відповів на питання.
                if (userNumber > answer) {
                    System.out.println("Your number is too big. Please, try again.");
                    System.out.println();
                    failedNumbers += " " + userNumber;
                } else if (userNumber < answer) {
                    System.out.println("Your number is too small. Please, try again.");
                    System.out.println();
                    failedNumbers += userNumber + ",";
                } else {
                    System.out.println("Congratulations, " + userName);
                    int[] failedNumbersInt = convertStringToIntArray(failedNumbers);
                    if (!failedNumbers.isEmpty()) {
                        System.out.print("Your numbers: ");
                        for (int num : failedNumbersInt) {
                            System.out.print(num + " ");
                        }
                        System.out.println();
                    }
                    failedNumbers = "";
                    System.out.println();
                }

            } while (userNumber != answer);
        }
    }

    // Конвертування строки в масив чисел
    public static int[] convertStringToIntArray(String str) {
        String[] strArray = str.split(",");
        int[] intArray = new int[strArray.length];

        if (strArray[0] != "") {
            for (int i = 0; i < intArray.length; i++) {
                intArray[i] = Integer.parseInt(strArray[i]);
            }
            return intArray;
        } else {
            return new int[]{};
        }
    }
}