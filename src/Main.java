import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        int randomNumber = random.nextInt(1, 6);
        int userNumber;
        int attempts = 0;

        do {
            System.out.println("Guess a number from 1 to 5");
            userNumber = scanner.nextInt();
            attempts++;
        } while (userNumber != randomNumber);

        System.out.println("You are winner! Attempts: " + attempts);

    }

    public static float sumNumbers(int a, float b) {
        return a + b;
    }
}